import * as React from 'react'

export const ZwOnline = (): JSX.Element => {
	return (
		<div>
			<iframe
				src="https://zw-one.glitch.me/"
				style={{
					width: '100%',
					height: '100%',
					backgroundColor: '#000000',
				}}
			></iframe>
		</div>
	)
}
