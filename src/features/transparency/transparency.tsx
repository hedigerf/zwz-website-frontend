import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Markdown } from '../markdown/markdown'

export const Transparency = (): JSX.Element => {
	const { t } = useTranslation('transparency')
	return (
		<div className="basic-paragraph">
			<Markdown>{t('transparency.content')}</Markdown>
		</div>
	)
}
