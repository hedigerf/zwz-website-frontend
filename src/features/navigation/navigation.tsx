import * as React from 'react'
import { LanguageSelector } from '../language/languageSelector'
import { NavLink, useLocation } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { useEffect, useRef, useState, WheelEventHandler } from 'react'
import { useGetStreamingQuery } from '../../services/live-service'

interface RouteDataItem {
	path: string
	label: string
}

const routes: RouteDataItem[] = [
	{
		path: '/about',
		label: 'route.about',
	},
	{
		path: '/gastro',
		label: 'route.gastro',
	},
	{
		path: '/organisation',
		label: 'route.organisation',
	},
	{
		path: '/principles',
		label: 'route.principles',
	},
	{
		path: '/awareness',
		label: 'route.awareness',
	},
	{
		path: '/covid',
		label: 'route.covid',
	},
	{
		path: '/workshop',
		label: 'route.workshop',
	},
	{
		path: '/contact',
		label: 'route.contact',
	},
	{
		path: '/host',
		label: 'route.host',
	},
	{
		path: '/transparency',
		label: 'route.transparency',
	},
	{
		path: '/jobs',
		label: 'route.jobs',
	},
]

export const Navigation = (): JSX.Element => {
	const { t } = useTranslation('navigation')

	const { data } = useGetStreamingQuery(undefined, {
		refetchOnFocus: true,
	})

	const navItems = useRef<HTMLDivElement>(null)
	const location = useLocation()
	const [placement, setPlacement] = useState<'top' | 'bottom'>('top')
	const [wwwLink, setWwwLink] = useState<'/' | '/zw-www'>('/')

	useEffect(() => {
		if (location.pathname === '/zw-www') {
			setPlacement('bottom')
			setWwwLink('/')
		} else {
			setPlacement('top')
			setWwwLink('/zw-www')
		}
	}, [location])

	const scrollNav: WheelEventHandler = (event) => {
		if (navItems && navItems.current) {
			navItems.current.scrollLeft += event.deltaY
		}
	}

	const streamNote = (
		<>
			<span className="-mt-px blink mr-1 red-circle" />
			<a href="https://www.twitch.tv/zentralwaescherei" target="_blank" className="no-underline" rel="noreferrer">
				Live Stream{' '}
			</a>
		</>
	)

	return (
		<div className={'uppercase navigation navigation-' + placement}>
			<div className={'nav-grid hide-scroll-bar'}>
				<div className="nav-mobile-hidden"></div>
				<div className="nav-small-hidden"></div>
				<div></div>
				<NavLink to={wwwLink} exact className={'nav-item w-full text-center'} activeClassName="active">
					ZW-WWW
				</NavLink>
				<div className="nav-small-hidden"></div>
				<div className="nav-mobile-hidden justify-center items-center">{data?.isLive && streamNote}</div>
				<div className="flex justify-center items-center">
					<LanguageSelector />
				</div>
			</div>

			<div ref={navItems} className={'nav-grid overflow-scroll hide-scroll-bar mt-1'} onWheel={scrollNav}>
				<div className="flex sticky left-0 pointer-events-auto nav-mobile-hidden nav-calendar">
					<NavLink to="/" exact className="nav-item flex-1 text-center" activeClassName="active">
						{t('route.calendar')}
					</NavLink>
				</div>

				<NavLink to="/" exact className="nav-item flex-1 text-center nav-mobile-show" activeClassName="active">
					{t('route.calendar')}
				</NavLink>

				<div className="nav-mobile-hidden"></div>

				{routes.map((route, index) => (
					<NavLink
						exact
						className="nav-item text-center pointer-events-auto"
						activeClassName="active"
						to={route.path}
						key={index}
					>
						{t(route.label)}
					</NavLink>
				))}

				<NavLink to="/imprint" exact className="nav-item flex-1 text-center nav-last-item" activeClassName="active">
					{t('route.imprint')}
				</NavLink>
			</div>
		</div>
	)
}
