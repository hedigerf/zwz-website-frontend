import * as React from 'react'

export const Logo = (): JSX.Element => {
	return (
		<div className="logo">
			<h1>ZW</h1>
		</div>
	)
}
