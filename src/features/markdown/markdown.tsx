import * as React from 'react'
import { ReactNode } from 'react'
import ReactMarkdown from 'react-markdown'
import { Link } from 'react-router-dom'

type MarkdownProps = { children: string }

export const Markdown = (props: MarkdownProps): JSX.Element => {
	const rewriteLink = ({ children, href }: { children: ReactNode & ReactNode[]; href?: string }) => {
		if (!href) {
			return <a>{children}</a>
		} else if (href.startsWith('/') && !href.startsWith('//')) {
			// route
			return <Link to={href}>{children}</Link>
		} else {
			// external link. open in new tab.
			return (
				<a target="_blank" rel="noreferrer" href={href}>
					{children}
				</a>
			)
		}
	}

	return <ReactMarkdown components={{ a: rewriteLink }}>{props.children}</ReactMarkdown>
}
