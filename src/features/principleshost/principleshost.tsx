import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Markdown } from '../markdown/markdown'

export const PrinciplesHost = (): JSX.Element => {
	const { t } = useTranslation('principleshost')
	return (
		<div className="basic-paragraph">
			<Markdown>{t('principleshost.pre')}</Markdown>
			<Markdown>{t('principleshost.principles')}</Markdown>
			<Markdown>{t('principleshost.after')}</Markdown>
		</div>
	)
}
