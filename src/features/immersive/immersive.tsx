import * as React from 'react'

export const Immersive = (): JSX.Element => {
	return (
		<div className="immersive-holder">
			<video autoPlay muted loop playsInline id="myVideo" className="immersive-video">
				<source src="assets/images/ImmersiveLoop.mp4" type="video/mp4"></source>
			</video>
			<div className="basic-paragraph immersive-layer">
				<a href="https://eventfrog.ch/immersive" className="immersive-tickets">Get Tickets on Eventfrog!</a>
				<div className="immersive-day">
					Tuesday 3.5 /<br></br> Wednesday 4.5
				</div>
				<div className="immersive-grid immersive-text">
					<div>Installation</div>
					<div>Laundromat</div>
					<div className="immersive-left">18:00</div>
					<div className="immersive-left">ZW-B</div>
				</div>
				<div className="immersive-grid immersive-text">
					<div>Workshop</div>
					<div>Lasse Nipkow</div>
					<div className="immersive-left">18:00</div>
					<div className="immersive-left">5. OG</div>
				</div>
				<div className="immersive-grid immersive-text">
					<div>Performance</div>
					<div>Groupe Nous</div>
					<div className="immersive-left">18:00-22:00</div>
					<div className="immersive-left">ZW-K</div>
				</div>
				<div className="immersive-grid immersive-text">
					<div>Movie</div>
					<div>Don`t Get Stuck in the Loop</div>
					<div className="immersive-left">21:00</div>
					<div className="immersive-left">5. OG</div>
				</div>
				<div className="immersive-day">Thursday 5.5</div>
				<div className="immersive-grid immersive-text">
					<div>Live</div>
					<div>Philipp Eden</div>
					<div className="immersive-left">19:00</div>
					<div className="immersive-left">ZW-H</div>
					<div> </div>
					<div>Nik Bärtsch`s Mobile</div>
					<div className="immersive-left">21:00</div>
					<div className="immersive-left">ZW-H</div>
				</div>
				<div className="immersive-grid immersive-text">
					<div>Installation</div>
					<div>Laundromat</div>
					<div className="immersive-left">18:00</div>
					<div className="immersive-left">ZW-B</div>
				</div>
				<div className="immersive-day">Friday 6.5</div>
				<div className="immersive-grid immersive-text">
					<div>Live</div>
					<div>Cie Across</div>
					<div className="immersive-left">18:00</div>
					<div className="immersive-left">5.OG</div>
					<div> </div>
					<div>Carl Gari & Abdullah Miniawy</div>
					<div className="immersive-left">20:00</div>
					<div className="immersive-left">ZW-H</div>
					<div> </div>
					<div>Eva Geist</div>
					<div className="immersive-left">22:00</div>
					<div className="immersive-left">ZW-H</div>
					<div> </div>
					<div>Angelo Repetto & Nico Sun</div>
					<div className="immersive-left">24:00</div>
					<div className="immersive-left">ZW-H</div>
				</div>
				<div className="immersive-grid immersive-text">
					<div>Installation</div>
					<div>Eigenklang</div>
					<div className="immersive-left">24:00</div>
					<div className="immersive-left">5.OG</div>
				</div>
				<div className="immersive-day">Saturday 7.5</div>
				<div className="immersive-grid immersive-text">
					<div>Live</div>
					<div>Casanora</div>
					<div className="immersive-left">19:00</div>
					<div className="immersive-left">ZW-H</div>
					<div> </div>
					<div>Die Wilde Jagd</div>
					<div className="immersive-left">21:00</div>
					<div className="immersive-left">ZW-H</div>
					<div> </div>
					<div>Cie Across</div>
					<div className="immersive-left">23:00</div>
					<div className="immersive-left">ZW-H</div>
					<div> </div>
					<div>InterSub</div>
					<div className="immersive-left">01:00</div>
					<div className="immersive-left">ZW-H</div>
				</div>
				<div className="immersive-grid immersive-text">
					<div>DJ</div>
					<div>Yuul</div>
					<div className="immersive-left">24:00</div>
					<div className="immersive-left">ZW-B</div>
					<div> </div>
					<div>Ryan James Ford</div>
					<div className="immersive-left">02:00</div>
					<div className="immersive-left">ZW-B</div>
					<div> </div>
					<div>Jamira Estrada</div>
					<div className="immersive-left">04:00</div>
					<div className="immersive-left">ZW-B</div>
				</div>
				<div> 
				<br></br>
					<p>Die Workshops von Lasse Nipkow sind in zwei Teile aufgeteilt:<br></br><br></br>
Am ersten Abend stellt er die wichtigsten Phänomene von 3D-Audio vor und zeigt das grosse Potential von 3D-Audio anhand von atemberaubenden Tonbeispielen auf, die er über ein Lautsprecher-Setup mit über 20 Neumann-Lautsprechern abspielt. Im Anschluss zeigt er einige Surround Sound 5.1 / 7.1 Produktionen und erklärt, was wir daraus lernen können.<br></br><br></br>
Am zweiten Abend zeigt Lasse Nipkow, was die wesentlichen Unterschiede zwischen Musik-Produktionen ohne Bild und Filmmusik-Produktionen sind. Weiter führt er vor, was für eine Rolle die Richtung von Schall spielt und wie das für 3D-Audio Produktionen genutzt werden kann. Schliesslich gibt er Einblick in seine Produktionen und präsentiert, wie sich die verschiedenen Klangelemente zu einem grossen Ganzen zusammensetzen.<br></br>
<br></br>
Das Publikum hat die Möglichkeit, sich während den Tonbeispielen im Raum zu bewegen und die vorgestellte Klangwelt aus verschiedenen Perspektiven zu erleben. Fragen sind willkommen!</p>
				</div>
			</div>
		</div>
	)
}
