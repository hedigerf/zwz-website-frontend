import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { useFormik } from 'formik'
import { ReCAPTCHA } from 'react-google-recaptcha'
import { useCreateIncidentMutation } from '../../services/awarness-service'
import { Incident } from '../../services/awarness.routes'

export const AwarenessForm = (): JSX.Element => {
	const { t } = useTranslation('forms')

	const [createIncdient, { isLoading: isUpdating }] = useCreateIncidentMutation()
	const formik = useFormik<Incident>({
		initialValues: {
			email: '',
			name: '',
			message: '',
		},
		onSubmit: (values, { resetForm, setSubmitting }) => {
			setSubmitting(true)

			if (!isUpdating) {
				const email = values.email === '' ? undefined : values.email
				const name = values.name === '' ? undefined : values.name
				createIncdient({
					email,
					name,
					message: values.message,
				})
					.unwrap()
					.then((fulfilled) => console.log(fulfilled))
					.catch((rejected) => console.log(rejected))
			}

			setTimeout(() => {
				setSubmitting(false)
				resetForm()
			}, 800)
		},
	})

	function onChange(value: string | null) {
		console.log('HEY')
		console.log('Captcha value:', value)
	}

	return (
		<div>
			<form onSubmit={formik.handleSubmit}>
				<input
					className="lineInput"
					id="name"
					name="name"
					type="text"
					autoComplete="off"
					onChange={formik.handleChange}
					value={formik.values.name}
					placeholder={t('form.name-awareness')}
				/>
				<input
					className="lineInput"
					id="email"
					name="email"
					type="email"
					autoComplete="off"
					onChange={formik.handleChange}
					value={formik.values.email}
					placeholder={t('form.email-awareness')}
				/>
				<textarea
					className="lineInput"
					id="message"
					name="message"
					autoComplete="off"
					rows={3}
					required
					onChange={formik.handleChange}
					value={formik.values.message}
					placeholder={t('form.message')}
				/>
				<button className="button" type="submit" disabled={formik.isSubmitting}>
					{t('form.submit')}
				</button>
			</form>
			<div>
				<ReCAPTCHA sitekey="6LfcJukcAAAAAB4ZlmsYpBLK3O2udBdpAgPTnfJb" onChange={onChange}></ReCAPTCHA>
			</div>
		</div>
	)
}
