import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { NavLink } from 'react-router-dom'
import { useAppSelector } from '../../app/hooks'
import { selectJoin } from '../join/joinSlice'

export const ImprintOverlay = (): JSX.Element => {
	const joinActive = useAppSelector(selectJoin)
	const { t } = useTranslation('imprintOverlay')

	const className = () => {
		let name = 'imprint-overlay'
		if (joinActive) name += ' inverted'
		return name
	}

	return (
		<NavLink to="/imprint" exact className={className()}>
			<div>{t('imprintOverlay.content')}</div>
		</NavLink>
	)
	//<div className={className()}>{t('imprintOverlay.content')}</div>
}
