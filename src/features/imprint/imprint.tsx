import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Markdown } from '../markdown/markdown'

export const Imprint = (): JSX.Element => {
	const { t } = useTranslation('imprint')
	return (
		<div className="basic-paragraph">
			<Markdown>{t('imprint.content')}</Markdown>
		</div>
	)
}
