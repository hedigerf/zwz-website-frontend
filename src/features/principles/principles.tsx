import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Markdown } from '../markdown/markdown'

export const Principles = (): JSX.Element => {
	const { t } = useTranslation('principles')
	return (
		<div className="basic-paragraph">
			<Markdown>{t('principles.content')}</Markdown>
		</div>
	)
}
