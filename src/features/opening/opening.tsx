import * as React from 'react'
import { CSSProperties } from 'react'
import { useTranslation } from 'react-i18next'
import { Markdown } from '../markdown/markdown'
import { Carousel } from 'react-responsive-carousel'

export const Opening = (): JSX.Element => {
	const { t } = useTranslation('opening')

	const arrowStyles: CSSProperties = {
		position: 'fixed',
		zIndex: 100,
		top: 'calc(50% - 15px)',
		width: '5%',
		minWidth: '45px',
		height: 'fit-contact',
		color: '#FFFFFF',
		cursor: 'pointer',
	}

	return (
		<div className="opening-outer">
			<Carousel
				showIndicators={false}
				showStatus={false}
				showThumbs={false}
				infiniteLoop={false}
				dynamicHeight={true}
				swipeable={false}
				swipeScrollTolerance={100}
				statusFormatter={(current, total) => `Current slide: ${current} / Total: ${total}`}
				renderArrowPrev={(onClickHandler, hasPrev, label) =>
					hasPrev && (
						<button
							type="button"
							onClick={onClickHandler}
							title={label}
							style={{
								...arrowStyles,
								left: 15,
							}}
						>
							<img src="assets/images/ZW-back.svg" alt="NEXT" />
						</button>
					)
				}
				renderArrowNext={(onClickHandler, hasNext, label) =>
					hasNext && (
						<button
							type="button"
							onClick={onClickHandler}
							title={label}
							style={{
								...arrowStyles,
								right: 15,
							}}
						>
							<img src="assets/images/ZW-next.svg" alt="NEXT" />
						</button>
					)
				}
			>
				<div className="opening-item">
					<div className="opening-one">
						<div className="opening-one-grid">
							<div>
								<img src="assets/images/ZW-orange.svg" alt="NEXT" className="opening-one-arrow" />
							</div>
							<div></div>
							<div className="opening-one-text-center">opening</div>
							<div className="opening-one-text-left">17.-20.11.2021</div>
							<div></div>
						</div>
						<div className="opening-one-logos">
							<div></div>
							<div>
								<img src="assets/images/Kanton.png" alt="Kanton Zürich Fachstelle Kultur" />
							</div>
							<div>
								<img src="assets/images/StadtZH.png" alt="Stadt Zürich Kultur" />
							</div>
							<div>
								<img src="assets/images/Migros.png" alt="Migros Kulturprozent" />
							</div>
							<div>
								<img src="assets/images/ErnstG.png" alt="Ernst Göhner Stiftung" />
							</div>
							<div></div>
						</div>
						<div className="opening-one-text">
							<div></div>
							<div>
								<Markdown>{t('opening.header')}</Markdown>
								<Markdown>{t('opening.text')}</Markdown>
							</div>
							<div></div>
						</div>
					</div>
				</div>

				<div className="opening-item">
					<div className="opening-grid">
						<div className="opening-day">MI, 17.11</div>
						<div className="opening-element">
							<div className="opening-time">19.00</div>
							<div className="opening-type">ZW-H-P</div>
							<div className="opening-title">
								chichiezha
								<div className="opening-text">
									<Markdown>{t('opening.chichiezha')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">20.00</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">21.00</div>
							<div className="opening-type">ZW-H-P</div>
							<div className="opening-title">
								titilayo
								<div className="opening-text">
									<Markdown>{t('opening.titilay')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">22.00</div>
							<div className="opening-type">ZW-K-DJ</div>
							<div className="opening-title">
								Yuul
								<div className="opening-text">
									<Markdown>{t('opening.yuul')}</Markdown>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div className="opening-item">
					<div className="opening-grid">
						<div className="opening-day">DO, 18.11</div>
						<div className="opening-element">
							<div className="opening-time">16.00</div>
							<div className="opening-type">ZW-K-W</div>
							<div className="opening-title">
								Workshop F96
								<div className="opening-text">
									<Markdown>{t('opening.workshopf96')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">17.00</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">18.00</div>
							<div className="opening-type">ZW-H-P</div>
							<div className="opening-title">
								NODIN
								<div className="opening-text">
									<Markdown>{t('opening.nodin')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">18.00</div>
							<div className="opening-type">ZW-K-R</div>
							<div className="opening-title">
								Megahex
								<div className="opening-text">
									<Markdown>{t('opening.megahex')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">19.00</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">20.00</div>
							<div className="opening-type">ZW-H-P</div>
							<div className="opening-title">
								DOROTA GAWEDA, EGLE KULBOKAITE
								<div className="opening-text">
									<Markdown>{t('opening.dorota')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">21.00</div>
							<div className="opening-type">ZW-B-K</div>
							<div className="opening-title">
								Emzyg
								<div className="opening-text">
									<Markdown>{t('opening.emzyg')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">22.00</div>
							<div className="opening-type">ZW-B-K</div>
							<div className="opening-title">
								ANGELO REPETTO, NICO SUN
								<div className="opening-text">
									<Markdown>{t('opening.nico')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">23.00</div>
							<div className="opening-type">ZW-K-DJ</div>
							<div className="opening-title">
								MAVICHAN, ELSA WURZEL OVA, 3APNHA
								<div className="opening-text">
									<Markdown>{t('opening.mavichan')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">00.00</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">01.00</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">02.00</div>
							<div className="opening-type">ZW-K-DJ</div>
							<div className="opening-title">
								Ju Dallas
								<div className="opening-text">
									<Markdown>{t('opening.judallas')}</Markdown>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div className="opening-item">
					<div className="opening-grid">
						<div className="opening-day">FR, 19.11</div>
						<div className="opening-element">
							<div className="opening-time">18.00</div>
							<div className="opening-type">ZW-K-R</div>
							<div className="opening-title">
								Megahex
								<div className="opening-text">
									<Markdown>{t('opening.megahex')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">19.00</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">20.00</div>
							<div className="opening-type">ZW-W</div>
							<div className="opening-title">
								Siebdruck Action
								<div className="opening-text">
									<Markdown>{t('opening.siebdruck')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">21.00</div>
							<div className="opening-type">ZW-B-P</div>
							<div className="opening-title">
								HOUSE OF B. PODEROSA VOGUE NIGHT
								<div className="opening-text">
									<Markdown>{t('opening.voguenight')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">21.00</div>
							<div className="opening-type">ZW-K-DJ</div>
							<div className="opening-title">
								Man in Sandals
								<div className="opening-text">
									<Markdown>{t('opening.maninsandals')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">22.00</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">23.00</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">00.00</div>
							<div className="opening-type">ZW-B-DJ</div>
							<div className="opening-title">
								HOUSE OF B. PODEROSA VOGUE NIGHT AFTERPARTY
								<div className="opening-text">
									<Markdown>{t('opening.voguenight')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">00.00</div>
							<div className="opening-type">ZW-K-DJ</div>
							<div className="opening-title">
								REGULA – ROTEL 4TTO, N0N UND ÄÖÜ (LIVE)
								<div className="opening-text">
									<Markdown>{t('opening.regula')}</Markdown>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div className="opening-item">
					<div className="opening-grid">
						<div className="opening-day">SA, 20.11</div>
						<div className="opening-element">
							<div className="opening-time">13.00</div>
							<div className="opening-type">ZW-K</div>
							<div className="opening-title">
								Kinderprogramm Kollektiv Neoele
								<div className="opening-text">
									<Markdown>{t('opening.neoele')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">14.00</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">15.00</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">16.00</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">17.00</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">18.00</div>
							<div className="opening-type">ZW-K-R</div>
							<div className="opening-title">
								Megahex
								<div className="opening-text">
									<Markdown>{t('opening.megahex')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">18.00</div>
							<div className="opening-type">ZW-K-W</div>
							<div className="opening-title">
								NeueNeueZeitung
								<div className="opening-text">
									<Markdown>{t('opening.nnz')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">19.00</div>
							<div className="opening-type">ZW-H-P</div>
							<div className="opening-title">
								Ive looked at love from both sides now oder ein permanenter zustand der erektion
								<div className="opening-text">
									<Markdown>{t('opening.erektion')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">20.00</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">21.00</div>
							<div className="opening-type">ZW-B-K</div>
							<div className="opening-title">
								Babylon
								<div className="opening-text">
									<Markdown>{t('opening.babylon')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">22.00</div>
							<div className="opening-type">ZW-B-K</div>
							<div className="opening-title">
								Heggi aka Naim
								<div className="opening-text">
									<Markdown>{t('opening.naim')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">23.00</div>
							<div className="opening-type">ZW-B-K</div>
							<div className="opening-title">
								Cobee
								<div className="opening-text">
									<Markdown>{t('opening.cobee')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">00.00</div>
							<div className="opening-type">ZW-H</div>
							<div className="opening-title">
								VIELZUHELL
								<div className="opening-text">
									<Markdown>{t('opening.vielzuhell')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">00.00</div>
							<div className="opening-type">ZW-B-DJ</div>
							<div className="opening-title">
								tvbxs
								<div className="opening-text">
									<Markdown>{t('opening.tvbxs')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">00.00</div>
							<div className="opening-type">ZW-K-DJ</div>
							<div className="opening-title">
								Sellyourmania
								<div className="opening-text">
									<Markdown>{t('opening.sellyourmania')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">01.00</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">02.00</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">02.00</div>
							<div className="opening-type">ZW-B-DJ</div>
							<div className="opening-title">
								Chris Korda
								<div className="opening-text">
									<Markdown>{t('opening.chriskorda')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">02.00</div>
							<div className="opening-type">ZW-K-DJ</div>
							<div className="opening-title">
								Siegwart
								<div className="opening-text">
									<Markdown>{t('opening.siegwart')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">03.00</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">04.00</div>
							<div className="opening-type">ZW-B-DJ</div>
							<div className="opening-title">
								DJ w.e.b.
								<div className="opening-text">
									<Markdown>{t('opening.djweb')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">04.00</div>
							<div className="opening-type">ZW-K-DJ</div>
							<div className="opening-title">
								Katzele
								<div className="opening-text">
									<Markdown>{t('opening.katzele')}</Markdown>
								</div>
							</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">05.00</div>
						</div>
						<div className="opening-element">
							<div className="opening-time">06.00</div>
							<div className="opening-type">ZW-B-DJ</div>
							<div className="opening-title">
								Amygdala
								<div className="opening-text">
									<Markdown>{t('opening.amygdala')}</Markdown>
								</div>
							</div>
						</div>
					</div>
				</div>
			</Carousel>
		</div>
	)
}
