import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Markdown } from '../markdown/markdown'
import { AwarenessForm } from '../forms/awarenessForm'

export const Awareness = (): JSX.Element => {
	const { t } = useTranslation('awareness')
	return (
		<div className="basic-paragraph">
			<Markdown>{t('awareness.content')}</Markdown>
			<AwarenessForm></AwarenessForm>
			<Markdown>{t('awareness.content-after')}</Markdown>
		</div>
	)
}
