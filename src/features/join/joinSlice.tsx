import { createSlice } from '@reduxjs/toolkit'
import type { RootState } from '../../app/store'
import { AnyAction, Reducer } from 'redux'

// Define a type for the slice state
interface JoinState {
	active: boolean
}

// Define the initial state using that type
const initialState = { active: false } as JoinState

export const joinSlice = createSlice({
	name: 'join',
	// `createSlice` will infer the state type from the `initialState` argument
	initialState,
	reducers: {
		toggle: (state) => {
			state.active = !state.active
		},
	},
})

export const { toggle } = joinSlice.actions

// Other code such as selectors can use the imported `RootState` type

export const joinReducer: Reducer<JoinState, AnyAction> = joinSlice.reducer

export const selectJoin: (state: RootState) => boolean = (state: RootState) => state.join.active
