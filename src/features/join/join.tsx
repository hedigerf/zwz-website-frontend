import * as React from 'react'
import { Button } from '../button/button'
import { useTranslation } from 'react-i18next'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { selectJoin, toggle } from './joinSlice'
import { useLocation } from 'react-router-dom'
import { useEffect, useState } from 'react'
import { Markdown } from '../markdown/markdown'
import { JoinForm } from '../forms/joinForm'

export const Join = (): JSX.Element => {
	const active = useAppSelector(selectJoin)
	const dispatch = useAppDispatch()
	const location = useLocation()
	const [showing, setShowing] = useState(true)

	useEffect(() => {
		if (location?.pathname === '/zw-www') setShowing(false)
		else setShowing(true)
	}, [location])

	const { t } = useTranslation('join')
	const getMore = () => {
		if (active) {
			return (
				<div className="join active">
					<Button onClick={() => dispatch(toggle())} secondary={active}>
						{t('buttonClose')}
					</Button>
					<div className="join-description">
						<Markdown>{t('main')}</Markdown>
						<JoinForm></JoinForm>
					</div>
				</div>
			)
		} else return
	}

	const className = () => {
		if (active) {
			return 'join active'
		} else return 'join'
	}

	if (!showing) return <></>
	return (
		<div className={className()}>
			<Button onClick={() => dispatch(toggle())} secondary={active}>
				{t('button')}
			</Button>
			{getMore()}
		</div>
	)
}
