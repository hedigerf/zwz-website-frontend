import * as React from 'react'
import { MouseEventHandler } from 'react'

type ButtonProps = {
	children: string
	secondary?: boolean
	className?: string
	onClick?: MouseEventHandler<HTMLDivElement>
}

export const Button = (props: ButtonProps): JSX.Element => {
	const cName = () => (props.secondary ? 'button secondary' : 'button') + ' ' + props.className

	return (
		<div onClick={props.onClick} className={cName()}>
			<span>{props.children}</span>
		</div>
	)
}
