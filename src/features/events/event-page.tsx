import * as React from 'react'
import { useParams } from 'react-router-dom'
import { useGetEventByIdQuery } from '../../services/events-service'
import { Event } from './event'
import { PartialEvent } from '../../services/events.routes'
import { useAppSelector } from '../../app/hooks'
import { selectLanguage } from '../language/languageSlice'
import { Helmet } from 'react-helmet'
import { baseUrl } from '../../services/base'

export const EventPage = (): JSX.Element => {
	const { id }: { id: string } = useParams()

	const { data, error, isLoading } = useGetEventByIdQuery({
		id,
		lang: useAppSelector(selectLanguage),
	})

	const content = () => {
		if (!data) return
		// Event does not extend PartialEvent => explicit transformation needed
		const imgUrl = (data.imgUrls && data.imgUrls[0]?.href) || undefined
		const partialEvent: PartialEvent = {
			...data,
			imgUrl,
			_links: {
				self: { href: `/api/events/${id}` },
				image: { href: `/api/images/${id}` },
			},
		}
		return <Event alwaysExpanded={true} hideShareLink={true} event={partialEvent}></Event>
	}

	return (
		<>
			<Helmet>
				<title>{data?.title + ' – Zentralwäscherei'}</title>
				<meta property="og:title" content={data?.title + ' – Zentralwäscherei'} />
				<meta name="description" content={data?.description} />
				{data?.imgUrls[0]?.href && <meta property="og:image" content={baseUrl + data?.imgUrls[0]?.href} />}
			</Helmet>
			<div className="event-single-view">
				{data && content()}
				{isLoading && 'Loading...'}
				{error && 'There was an error'}
			</div>
		</>
	)
}
