import * as React from 'react'
import { useGetEventByIdQuery } from '../../services/events-service'
import { PartialEvent } from '../../services/events.routes'
import { Markdown } from '../markdown/markdown'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { baseUrl } from '../../services/base'
import { useAppSelector } from '../../app/hooks'
import { selectLanguage } from '../language/languageSlice'
import { LazyLoadImage } from 'react-lazy-load-image-component'

type EventDetailProps = { event: PartialEvent; hideShareLink?: boolean; hover: boolean; shown: boolean }

const Text = ({ id }: { id: string }) => {
	const { data, error } = useGetEventByIdQuery({
		id,
		lang: useAppSelector(selectLanguage),
	})
	return (
		<>
			<Markdown>{data?.description || ''}</Markdown>
			{error}
		</>
	)
}

export const EventDetail = ({ event, hideShareLink, shown, hover }: EventDetailProps): JSX.Element => {
	const { t } = useTranslation('event')
	if (!event.id) return <div>event id is undefined</div>

	const shareLink = () => {
		if (!hideShareLink) return <Link to={`/event/${event.id}`}>{t('event.permalink')}</Link>
		else return undefined
	}

	const image = (cssClassName: string) => {
		const maybeImgTag = event.imgUrl ? <LazyLoadImage src={baseUrl + event.imgUrl} /> : ''
		return <span className={cssClassName}>{maybeImgTag}</span>
	}

	return (
		<div className="event-detail">
			<div>
				{shown && (
					<div>
						<Text id={event.id!}></Text>
						{shareLink()}
					</div>
				)}
			</div>
			{(hover || shown) && image(hover && !shown ? 'event-image-hover' : '')}
		</div>
	)
}
