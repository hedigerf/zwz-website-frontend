import * as React from 'react'
import { useEffect, useState } from 'react'
import { addDays, addWeeks, getUnixTime } from 'date-fns'
import { useTranslation } from 'react-i18next'
import Select, { components, DropdownIndicatorProps, GroupBase, OnChangeValue, StylesConfig } from 'react-select'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { filter as applyFilter } from './eventSlice'
import { selectLanguage } from '../language/languageSlice'

type OptionType = { value: string; label: string }

const DropdownIndicator = (props: DropdownIndicatorProps<OptionType, false>) => {
	return (
		<components.DropdownIndicator {...props}>
			<span>↓</span>
		</components.DropdownIndicator>
	)
}

const customStyles: StylesConfig<OptionType, false, GroupBase<OptionType>> = {
	option: (provided, state) => ({
		...provided,
		backgroundColor: state.isSelected ? 'black' : 'white',
		border: '2px solid black',
		borderRadius: '22px',
		padding: '0.2em 0.5em',
		textTransform: 'uppercase',
		color: state.isSelected ? 'white' : 'black',
		marginTop: '0.2em',
		'&:hover': {
			backgroundColor: 'black',
			color: 'white',
		},
	}),
	input: (provided) => ({
		...provided,
		margin: '0',
		padding: '0',
	}),
	placeholder: (provided) => ({
		...provided,
		margin: 0,
		padding: '0.2em 0.5em',
	}),
	menu: (provided) => ({
		...provided,
		border: 'none',
		boxShadow: 'none',
		background: 'none',
		textAlign: 'center',
		padding: 0,
		margin: 0,
	}),
	menuList: (provided) => ({
		...provided,
		padding: 0,
		margin: 0,
	}),
	indicatorSeparator: () => ({}),
	clearIndicator: () => ({ display: 'none' }),
	dropdownIndicator: (provided, state) => ({
		...provided,
		marginLeft: '-2em',
		padding: '0',
		color: state.hasValue ? 'white' : 'black',
		'&:hover': { color: 'white' },
	}),
	control: (provided, state) => ({
		...provided,
		textTransform: 'uppercase',
		textAlign: 'center',
		padding: '0',
		margin: '0',
		border: '2px solid black',
		borderRadius: '22px',
		minHeight: '34px',
		boxShadow: 'none',
		backgroundColor: state.hasValue ? 'black' : 'white',
		'&:hover': { borderColor: 'black' },
	}),
	valueContainer: (provided) => ({
		...provided,
		margin: 0,
		padding: 0,
	}),
	singleValue: (provided, state) => {
		const opacity = state.isDisabled ? 0.5 : 1
		const transition = 'opacity 300ms'

		return {
			...provided,
			padding: '0.2em 0em',
			color: state.hasValue ? 'white' : 'black',
			opacity,
			transition,
		}
	},
}

const options: OptionType[] = [
	{
		value: 'archive',
		label: 'archive',
	},
	{
		value: 'allEvents',
		label: 'allEvents',
	},
	{
		value: 'thisWeek',
		label: 'thisWeek',
	},
]

export const EventFilter: React.FunctionComponent<Record<string, never>> = () => {
	//const { data, error, isLoading } = useGetEventLocation()
	const dispatch = useAppDispatch()
	const currentLang = useAppSelector(selectLanguage)

	const { t, ready } = useTranslation('filter')
	const [opt, setOpt] = useState<OptionType[] | undefined>()

	const getFilter = (key: string) => {
		const now = new Date()
		if (key === 'archive') {
			return {
				from: 0,
				to: getUnixTime(addDays(now, 1)),
			}
		} else if (key === 'thisWeek') {
			return {
				from: getUnixTime(now),
				to: getUnixTime(addWeeks(now, 1)),
			}
		} else {
			return {
				from: getUnixTime(now),
				to: 0,
			}
		}
	}

	useEffect(() => {
		if (ready) {
			const mapped = options.map(({ value }) => ({
				value,
				label: t('filter.' + value),
			}))
			setOpt(mapped)
		}
	}, [ready, currentLang])

	// not happy with hardcoded filters, but ok for now.
	const onValue = (res: OnChangeValue<OptionType, false>) => {
		const filter = getFilter(res?.value || '')
		dispatch(applyFilter(filter))
	}

	if (ready && opt) {
		return (
			<div className="grid grid-cols-3 md:grid-cols-5 lg:grid-cols-7 mt-1">
				<Select
					className={'col-span-2'}
					defaultValue={opt[1]}
					options={opt}
					styles={customStyles}
					isClearable
					onChange={(newValue) => onValue(newValue)}
					isSearchable={false}
					components={{ DropdownIndicator }}
				></Select>
			</div>
		)
	} else return <></>
}
