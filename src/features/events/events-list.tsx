import * as React from 'react'
import { useGetEventsQuery } from '../../services/events-service'
import { Event } from './event'
import { useAppSelector } from '../../app/hooks'
import { selectFrom, selectTo } from './eventSlice'
import { EventFilter } from './event-filter'
import { Events } from '../../services/events.routes'
import { selectLanguage } from '../language/languageSlice'

export const EventsList = (): JSX.Element => {
	const from = useAppSelector(selectFrom)
	const to = useAppSelector(selectTo)
	const { data, error, isLoading } = useGetEventsQuery({
		lang: useAppSelector(selectLanguage),
		from,
		to,
	})

	const renderEvents = (data: Events) => {
		const events = data._embedded.events
		return events.map((event) => {
			return (
				<div key={event.id} className="events-container">
					<Event key={event.id} event={event} />
				</div>
			)
		})
	}

	return (
		<div className="events-list">
			<EventFilter />
			{error ? <>Oh no, there was an error</> : isLoading ? <>Loading...</> : data ? <>{renderEvents(data)}</> : null}
		</div>
	)
}
