import React from 'react'
import { useTranslation } from 'react-i18next'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { changeLanguage, selectLanguage } from './languageSlice'

export const LanguageSelector = (): JSX.Element => {
	const dispatch = useAppDispatch()
	const { i18n, t } = useTranslation('general')
	const currentLang = useAppSelector(selectLanguage)

	const changeLanguageLocal = () => {
		const newLanguage = currentLang === 'de' ? 'en' : 'de'
		i18n.changeLanguage(newLanguage)
		dispatch(changeLanguage(newLanguage))
	}

	return (
		<div onClick={changeLanguageLocal} className="languageSelector">
			{currentLang === 'de' ? t('lang.en') : t('lang.de')}
		</div>
	)
}
