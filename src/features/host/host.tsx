import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Markdown } from '../markdown/markdown'

export const Host = (): JSX.Element => {
	const { t } = useTranslation('host')
	return (
		<div className="basic-paragraph">
			<Markdown>{t('host.content')}</Markdown>
		</div>
	)
}
