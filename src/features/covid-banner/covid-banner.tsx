import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { NavLink } from 'react-router-dom'

export const CovidBanner = (): JSX.Element => {
	const { t } = useTranslation('covid')

	return (
		<div className={'banner-position'}>
			<div></div>
			<div></div>
			<div className={'covid-grid hide-scroll-bar'}>
				<NavLink to="/immersive" exact className="nav-item covid-banner text-center" activeClassName="active">
					{t('covid.banner')}
				</NavLink>
			</div>
		</div>
	)
}
