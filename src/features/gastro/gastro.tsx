import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { GastroMenu } from '../gastro-menu/gastro-menu'
import { Markdown } from '../markdown/markdown'

export const Gastro = (): JSX.Element => {
	const { t } = useTranslation('gastro')
	return (
		<div className="basic-paragraph">
			<GastroMenu></GastroMenu>
			<Markdown>{t('gastro.post-menu')}</Markdown>
		</div>
	)
}
