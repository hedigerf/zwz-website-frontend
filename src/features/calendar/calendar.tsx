import * as React from 'react'
import { EventsList } from '../events/events-list'

export const Calendar = (): JSX.Element => {
	return (
		<div>
			<EventsList />
		</div>
	)
}
