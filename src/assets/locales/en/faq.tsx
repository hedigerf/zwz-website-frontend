export default {
	'faq.content': `- **How do you finance yourselves?**
- Mention the city's rent waiver, the building loan (and link to the municipal council's decision), then mention foundations and last but not least say that people can donate to us at any time.
---
- **How much rent do you pay?**
- No, we do not pay rent due to the previously mentioned city council decision. This allows us to offer the space and the catering at the best possible conditions. We strive, and according to our statutes are also obliged, to reinvest any profits in the project. However, should we make a profit, we would have to pay rent to the city.
---	
- **Where can I find your statutes?**
- You can find our statutes [here](/transparency). In addition, we are registered in the [commercial register]().
---
- **Can I reserve your location for my wedding?**
- No, unfortunately not. All our events must have a public character, unless they concern social groups for which a protected setting must be created. You can find more information about our program [here](/host).
---
- **What is your relationship with the city?**
- We run the space and take over the space on a utility loan from Raumbörse/City of Zurich.  The city of Zurich supports us with a tenant's allowance and a building loan (GR decision). Otherwise, we are an association independent of the city.
---
---
If you have any further questions, please [contact us](/contact).`,
}
