export default {
	'awareness.content': `Awareness means - consciousness. We as the association Zentralwäscherei want to be conscious on the road. Whether club members, performing artists, or guests we should all be aware at all times what our actions can trigger in other people. This includes statements that are sexist, racist, ableist, homo- or transphobic, as well as harassment of any kind.

We respect the boundaries of our fellow human beings as well as our own. Often we are accompanied by patterns that are not easy to discard. Nevertheless, we expect everyone who is in the premises of the Verein Zentralwäscherei to actively work on it, to exemplify awareness and also to implement it.

---

*Have you been on the premises of the ZW and had an experience that you would like to share or discuss with us?*

The digital mailbox is there for suggestions, criticism and personal experiences. All contributions will be read by our awareness team and if there is a wish, a contact will follow. Feedback is also possible anonymously. Write to us in the language you feel most comfortable with.`,
	'awareness.content-after': `---
- You can find our awareness guide [HERE](https://drive.google.com/file/d/1m2mfzj-MTPw9GdHaGCd1q35d8JG37QZz/view?usp=sharing)
- Status November 2021`,
}
