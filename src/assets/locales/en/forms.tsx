export default {
	'form.name': 'NAME',
	'form.name-awareness': 'NAME - OPTIONAL',
	'form.email': 'EMAIL',
	'form.email-awareness': 'EMAIL - OPTIONAL',
	'form.message': 'MESSAGE',
	'form.submit': 'SUBMIT',
	'form.sucess': 'Successfully submitted.',
}
