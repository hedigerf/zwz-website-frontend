export default {
	'opening.content': 'Opening',
	'opening.header': `We are Verein Zentralwäscherei. We are opening a non-commercial cultural space in Kreis 5 for everyone to participate in. Join us for the opening festival and beyond.`,
	'opening.text': `Admission only possible with Covid certification. We are also required to collect your data for contact tracing. They will be deleted after 14 days. *[COVID Info](/covid)*

No advance sale. Tickets are available at the box office for a guide price. (For more info on the guide price, see _[About Us](/about)_). First come, first save. Entry from 18 y. at party night at the festival.npm

Please behave as you would wish others to behave. Read our _[awareness concept](/awareness)_.`,
	'opening.chichiezha': `**"drzwüsche"**

A piece that deals with the challenge of creating an identity in the duality of cultures. It is a reappraisal of experiences and an exchange of experiences.
	
It is a tribute to all those who feel "drzwüsche"- in between-, for those with a migration background who are learning to balance between the culture of their parents and the Swiss culture, for those whose parents come from two different countries and feel at home or/and foreign in both. We are all in some form in between, because no identity is static. The feeling of belonging everywhere and nowhere completely is familiar to most.
	
This piece was choreographed by Anna Chiedza Spörri and Muhammed Kaltuk and after its premiere at Bestival in 2019, it was also performed at Theater Basel and Dampfzentrale. Now it is on tour in a short version and in pairs, questioning miccroaggressions and racism in everyday life with text and dance. 
	
**Annakatharina Chiedza Spörri**
	
Anna has been active in the Bernese dance scene for over 10 years. She started with Hip- Hop, House and Ragga and developed these styles all extensively. In 2013 she visited the Peri Dance School in New York. She founded the dance groups "Urban Rebels" and "Unusual". She is also co-initiator of "Limelight", a platform for young choreographers in Switzerland. 
	
Since 2019, she has been a guest choreographer with various pieces at Theater Basel, Tojo Theater and Dampfzentrale Bern. 
	
**Sophie Gerber**
	
Sophie graduated last year from a five-year sports high school with a focus on dance. During this time she danced intensively and continued her education in various styles. Since two years she is active as a professional dancer in Switzerland and has already danced in various productions. Since September 2020, she is studying at the Höhere Fachschule für Zeitgenössischen und Urbanen Bühnentanz in the Tanzwerk.`,
	'opening.titilay': `Titilayo Adebayo is a non-conforming performer, movement and dance artist in flux with emergence. Emerging from where? Emerging towards what? Painting in rhythm? Speaking without words? Who knows? Does it matter? They aim to bring a holistic approach to creating and experiencing performance, visual and experiential art. Making performance from a place of flow. Their practice lies in these principles.`,
	'opening.yuul': `Yuul aka Yael Weinberg is a young talented DJ from the Regula - collective. Her style can be described as eclectic, as she feels comfortable in several genres and is not afraid to deconstruct them. `,
	'opening.workshopf96': `**Music Production Workshop by F96**

In this workshop various experienced artists give insight into the production of electronic music. Take your first steps with step sequencers and get comfortable with filter banks. No prior knowledge is required – this workshop is meant to be an ice breaker!

**In concrete terms, the aim is to learn the following skills:**
	
- Know the basics of creating your own track (beats, basslines, melodies, sampling)
- Set up a new project in Ableton (popular software for electronic music production)
- Experiment with drum machines and synthesizers
- Create a project together with a mentor
---
Basic equipment will be provided. Participants are asked to bring a laptop and headphones if they have them. If you cannot bring a laptop, don’t hesitate to get in touch and we will find a solution. Synthesizers and drum machines will also be provided for screenless jams. 

**Timetable**

- 16:00 - 16:45  Introduction 
- 16:45 - 17:30  Set up a project with a mentor 
- 17:30 - 18:30  Free time/work on tracks/jam with hardware 
- 18:30 - 19:00  Reflection in the clubraum 
---
**Registration**
	
Space in the workshop is limited (max 15.), please sign up ahead of time: ***[Registration form](https://docs.google.com/forms/d/e/1FAIpQLSeMWlXYgcvjqeQoxyj2Y_uezLsYT05DvdXBvRkfAY0msO0g7A/viewform?usp=sf_link)***
If you cannot attend or need to cancel your registration, please let us know 1 week prior to the workshop so that we can give your space to someone on the waitlist. Priority will be given to FINTA, LGBTQIA+, Queer, or BiPoc identifying persons. 
	
**Call for Mentors/Equipment**
	
If you have equipment or music producing knowledge to contribute, _[please fill out the Mentor and Equipment Sign Up Form](https://forms.gle/P3LZHkqQMsTVURhG8)_:  Mentors will paired with 2-3 participants each to help set-up an Ableton project and answer questions and support as needed. As a small token of our gratitude, Mentors will be compensated with a pass to the full ZZZZestival.
	
**F96**
	
F96 is a feminist collective. They demand a diverse cultural life and strive to actively shape it. They motivate and support themselves and others in thinking and implementing ideas and in sharing and expanding knowledge.

F96 grows and shrinks continuously and dynamically - FINTA* who want to actively shape the collective are always welcome. We stand on the shoulders of all those who came before us and want to reshape the future together with different actors.`,
	'opening.nodin': `**Work-in-Progress Sharing: "I am many, I contain multitudes."**

- _The spine is surrounded by fluid_
- _And pulsates in different rhythms._
- _Cerebrospinal fluid amen. Dura mater amen._

---

_I am many, I contains multitudes._ Four performers share one space, but they are much more. Their multitudes are numerous, arise in intraaction1 and are not held in the individual. A stop, a pause; feelings surface, impulses intensify, views are clarified. The performers share these moments. In the intraaction, their identities liquefy, seek hold in the wet. Trauma of the leak. My feelings spill over. My feelings float over.

---
- _Liquor cerebrospinalis amen. Dura mater amen._
- _Listening to the different currents,_
- _looking for a shared impulse, praying_
- _for a common morality._
---

In the performance I am many. I contains multitudes. the collective "noDIN" search for identities in the process of sharing. What can we learn from sharing? - About our needs, our own contradictions, our ambiguities?
	
1 The term comes from Karen Barad. It can be understood as an updated version of the concept of interaction and at the same time introduces an alternative view of the world in which there are no autonomous individuals interacting with each other, but in which all beings only become together through their differences as they encounter each other.
	
**noDIN**

- 4 performers together form the noDIN collective.
- Mira Studer, Yadin Bernauer, Dustin Kenel and Sophie Germanier.
- Their different disciplines and ways of working collide, merge and divide again.
---
**Credits**

The project is accompanied by Danse & Dramaturgie (D&D CH), a program initiated by Théâtre Sévelin 36 Lausanne, in collaboration with Dampfzentrale Bern, ROXY Birsfelden, Südpol Luzern, Tanzhaus Zürich, TU-Théâtre de l'Usine Genève; supported by Pro Helvetia and SSA Société Suisse des Auteurs.
	`,
	'opening.megahex': `Megahex is a subjective, political, radical and decentralized internet radio from Zureich.  Megahex informs about leftist resistance struggles, clarifies grievances and broadcasts music shows by independent collectives/artists. For the ZW Opening Megahex takes over the streaming hosting and also produces three own broadcasts live! `,
	'opening.xerosophie': `More info coming soon.`,
	'opening.emzyg': `Emzyg is a five-piece band from Zurich, fresh but extremely stylish. A mix of dreamy psychedelic, garage and post-punk that you can best enjoy with your sunglasses on.`,
	'opening.dorota': `Dorota Gawęda (PL) and Eglė Kulbokaitė (LT) are an artist duo based in Basel (CH). Both are 2012 graduates of the Royal College of Art in London. Their work spans performance, sculpture, painting, photography, fragrance and video. Gawęda and Kulbokaitė are the recipients of the Swiss Performance Art Award 2021 and among the finalists of the Swiss Art Awards 2021, they have also been nominated for Prix Mobilière 2022. They have recently published their first monograph with Edizioni Periferia and Pro Helvetia and are the founders of YOUNG GIRL READING GROUP (2013– 2021).`,
	'opening.nico': `**Our in-house libero Nico Sun now finally in the role he has secretly worked so hard for: himself in the spotlight. A little modesty remains, however, he gets support from Angelo Repetto, one of the had of the Zurich electropop duo "Wolfman", who has had a little more time to practice lately. Together they present selected pieces of their synthesizer collection and play a live set between Krautrock and Ambient. `,
	'opening.mavichan': `More info coming soon.`,
	'opening.judallas': `Ju Dallas is a young, upcoming DJ talent from Zurich. She is active in various projects, such as the label and platform Axophono together with Sean Douglas, Anaram Records and also recently as booker at the Club Zukunft. Certainly someone that  we will hear a lot about in future.`,
	'opening.siebdruck': `More info coming soon.`,
	'opening.voguenight': `The House of B. Poderosa is hosting this Kiki Vogue Night.

Vogue nights are part of the ballroom scene, which was started by black and latinx, trans and queer communities in the 70s in New York. As this is a Vogue Night and not a Ball, there is no specific theme, so you have to spend less time preparing and can just come and show your skills to the judges in whatever you feel fierce in.

The categories are: Runway OTA, Vogue Femme OTA, Shake that Ass OTA, Face OTA

After the categories we have a great party for you with our DJs Klair-Alice and DJ Essengo.`,
	'opening.maninsandals': `Maxi - man in sandals - once placed behind turntables in Birkenstöcken, leads highly concentrated through his collection of earthy ambient.`,
	'opening.afterparty': `More info coming soon.`,
	'opening.regula': `Regula is a fairly big group of friends who got together to organize happenings around electronic music. They realize events that meet their own musical, creative and communal ideas. From the group, several people are engaged within electronic music, either as live musicians, DJs or producers. Regula’s musical creation is not so much confined by genre boundaries but more by aesthetic ideas, some are shared others are more individual. Coming from rave and club culture, these musical roots are still relevant and mark the centrepiece in most of our work but as our sonical fingerprint is evolving, we find interest in diverse forms of musical creation and presentation including sonic installations, ambient productions and experimental listening events.`,
	'opening.neoele': `**Glitzerwunsch**

Sparkle, shimmer and glitter in all kinds of colors - together we will make a fairy-tale world in a glass and let the snow swirl around in the fascinating glitter glass and enchant us with it.
	
At 3.30 p.m. we will tell the fairy tale *Sterntaler* (Brothers Grimm) for all our little visitors and of course for the big ones as well. The fairy tale is about helpfulness, trust, courage and community spirit.`,
	'opening.erektion': `**Ive looked at love from both sides now oder ein permanenter zustand der erektion**
- What is love, baby don’t hurt me, don’t hurt me, no more ...
- You can look at me. I want you to look at me. My body is ready to be watched, consumable. Available. Sit down. Look at me. Dive into a paradise of never-ending love experience. Search no more. We are what you crave. This world is perfect und shiny.
- Tasty Songs, tasty bodies, tasty action – come and look at our faces!
---
A peformance about love from and with Joel Kammermann, Nils Nikolas Klaus, Isabella Roumiantsev, Stefanie Steffen, Marie Nest, Janna Rottmann, Fynn Malte Schmidt, Timo Raddatz und Emanuel Steffen`,
	'opening.nnz': `The crowning glory of the Zurich newspaper landscape prints its third issue. Yes and now? Neue Neue Zeitung makes soft-vernissage on Saturday, the first time live to touch. Hänki, Mara, Sophie and Jeannie will be sitting up and reading from their free newspapers, which you can buy exclusively at a soliprice of 5.-. Most importantly, you can chat and laugh with them and do other fun stuff. There are temporary tattoos and memories that will last forever.`,
	'opening.babylon': `Babylon Music is a young music collective from the heart of Zurich, consisting (among others) of the three musicians Akira, P Vlex and Jaron Ivy. From the creation of the beats to the concept of the live shows to the editing of the videos: Babylon Music keeps everything DIY and in the family. Meanwhile, Babylon has a considerable catalog of productions, in which "Flores" is their latest release.`,
	'opening.naim': `Basel artist Heggi aka Naim manages to sound different in every project without losing his signature. In his music, hip-hop and trap elements meet electronic and also rock influences. Aesthetically, he plays with different subcultures - once he named Led Zeppelin  as a big inspiration - which underlines his musical openness.`,
	'opening.cobee': `With his first single releases and his debut album "Chaos", Cobee quickly explored the boundaries of dialect rap. Now he's trying out new directions, both linguistically and musically. The beats are more reduced, undertake with pleasure sometimes digressions away from the classical Trapnarrativ. The lyrics are new in High German, which sounds universal, and certainly should sound. A master of chaos, genre descriptions do extremely difficult (Trap-Cloud-Hop-Free-Flow!), to hear live for the first time with his new project!`,
	'opening.tvbxs': `TVBXS is a very engaged person in Zurichs nightlife and a great DJ. She is part of the collective TBD, which creates different platforms in the spectre of nightlife for less privileged people. Her party night "putasfinas" follows the same socio-political activism and tries to include diverse people on different fronts.`,
	'opening.vielzuhell': `The light and visual collective VielZuHell, consisting of the artists Timo Raddatz, Hannah Gottschalk and Lukas Matthys, has its base in Zurich's Photobastei, from where they bring the wildest solutions for light design and visuals out into the world. From concerts of various genres to techno raves and art installations, their work is always characterized by abstraction and audio reactivity, and the resulting symbiosis of musical and visual perception.`,
	'opening.sellyourmania': `Sellyourmania will sell you all pragmatically some: leftfield electro/breakbeat und halftime acid! Be Aware.`,
	'opening.chriskorda': `Chris Korda (born 1962) is an American antinatalist activist, transgendered suicide cult leader, electronic music composer, digital artist,  free software developer and vegan leader of the Church of Euthanasia. Church of of Euthanasia is a Massachusetts-based organization that advocates halting the overpopulation of the Earth by its four pillars of "Suicide, Abortion, Cannibalism and Sodomy."`,
	'opening.siegwart': `For what feels like an eternity, Zurich's Siegwart has been playing the clubs in and around Zurich. He is considered a Djs Dj and always impresses with his sensitively curated sets. From cosmic to edgy.`,
	'opening.djweb': `Walid El Barbir aka DJ W.E.B is part of the Les Points, Mikro collective and founder of the WEBarchive. W.E.B. is known as being a very obscure, genre-bending producer and dedicated niche genre explorer. Together with his comrades he pioneered a genre-mix which has found mass adaption years later and has spread in to moments of activations for many younger people - mostly through organizing & playing illegal raves at Mikro.`,
	'opening.katzele': `Founder of Malka Tuti records from Berlin, Katzele is slowly building a reputation for long lysergic sets behind the decks, weaving layers of drones, trippy melodies, esoteric concepts and slow deep beats, all fused together in creating long sonic dancefloor journeys.`,
	'opening.amygdala': `Anissa Todesco aka Amygdala is a Swiss DJ, producer, multi-instrumentalist and record collector. Unrestricted by genre or tempo, Amygdala’s tracks and DJ sets alike are a phantom on fire. Her style combines proto-acid punk, early leftfield music and drum & bass along with infectious techno rhythms and more aggressive industrial bops.`,
}
