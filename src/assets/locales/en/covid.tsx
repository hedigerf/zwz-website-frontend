export default {
	'covid.banner': `Please read our COVID-INFO before you visit us!`,
	'covid.link': `Covid Info`,
	'covid.content': `**As of February 17, 2022, the mask and certificate requirement at ZW is lifted.**

---
- **Remember:**
- • Vaccinated people can still spread Covid-19 - protection from transmissibility decreases significantly after 2-3 months after becoming fully vaccinated
- • Covid-19 can lead to serious illness

---
- **Reduce the spread of Covid-19:**
- • Please wash and disinfect your hands regularly and thoroughly
- • If you are sick or have symptoms, please stay at home and do a self-test (even if you are vaccinated)
- • If possible, keep your distance (in line, at the bar, etc.).`,
}
