export default {
	'gastro.pre-menu': `The Kleinwäscherei (ZW-K) is the hub of the cultural space «Zentralwäscherei». Here you will find our bistro where you will be fed culinary & cultural.

A place for relaxed & animated discussions, a place of togetherness, Pole of calm & melting pot.  
	
Rather quiet during the day, rather noisy in the evening.
	
We serve lunch from tuesday to friday and we serve dinner from tuesday to saturday.

---
- **Reservations**
- [reservieren@zentralwaescherei.space](mailto:reservieren@zentralwaescherei.space)
	
---
- **Opening Hours**
- Monday: Closed
- Tuesday: 11:30 - 23:00
- Wednesday: 11:30 - 23:00
- Thursday: 11:30 - 03:00
- Friday: 11:30 - 03:00
- Saturday: 13:00 - 03:00
- Sunday: 16:00 - 23:00`,
	'gastro.gastro-title': '**GASTRO**',
	'gastro.gastro-info':
		'The Kleinwäscherei (ZW-K) is the hub of the cultural space «Zentralwäscherei». Here you will find our bistro where you will be fed culinary & cultural.',
	'gastro.menu-title': '**OUR MENU**',
	'gastro.menu-info':
		'Our lunch kitchen is open during the week from Tuesday to Friday. Our evening kitchen is open from Tuesday to Saturday.',
	'gastro.wochenmenu-title': '**WEEKLY MENU 17 - 21. May**',
	'gastro.wochenmenu': 'Miso glazed eggplant with tomato bulgur and tahini yogurt',
	'gastro.dienstag-title': '**TUESDAY 17.05**',
	'gastro.dienstag': 'Zucchini cakes with feta and lentil rice',
	'gastro.mittwoch-title': '**WEDNESDAY 18.05**',
	'gastro.mittwoch': `Risotto with fresh peas, red onions, lemon, sorrel and Belper tuber`,
	'gastro.donnerstag-title': '**THURSDAY 19.05**',
	'gastro.donnerstag': `Green asparagus on baked potatoes, monk's beard, kumquat, tree nut and aïoli`,
	'gastro.freitag-title': '**FRIDAY 20.05**',
	'gastro.freitag': `Fresh glass noodle salad with lemongrass tofu, mushrooms, cucumber and coriander`,
	'gastro.samstag-title': '**SATURDAY 21.05**',
	'gastro.samstag': `Handmade falafel with pita bread, eggplant caviar and pickles`,
	'gastro.drinks-title': '**DRINK**',
	'gastro.drinks':
		'The Kleinwäscherei offers space for drinks and relaxed or animated discussions. A place of togetherness. Pole of calm & melting pot. Rather quiet during the day, rather loud in the evening.',
	'gastro.work-title': '**WORK**',
	'gastro.work':
		'Tuesday to Saturday afternoon you will always find a quiet corner for meetings or work. The chai is ordered, until then you can sharpen your senses with fine herbal teas from Al Canton or a Cafe Noir.',
	'gastro.post-menu': `---
- **Reservations**
- [reservieren@zentralwaescherei.space](mailto:reservieren@zentralwaescherei.space)
	
---
- **Opening Hours**
- Monday: Closed
- Tuesday: 11:30 - 23:00
- Wednesday: 11:30 - 23:00
- Thursday: 11:30 - 03:00
- Friday: 11:30 - 03:00
- Saturday: 13:00 - 03:00
- Sunday: 16:00 - 23:00

---
- **Holiday Opening Hours 2022**
- *New Year*
- 1.1. 07:00 - 23:00 (dayrave)
- 2.1. Closed
- *Easter*
- Fri 15.4. 17:30 - 02:00 evening kitchen open
- Sat 16.4. 17:30 - 06:00 evening kitchen open
- Sun 17.4. PingPongTournament 13:00 - 23:00
- *1st of Mai*
- Sun, 1.5. closed - everyone to the Demo!
- *Ascension*
- Thur 26.5. 17:30 - 02:00 evening kitchen open
- Fri 27.5. 17:30 - 02:00 evening kitchen open`,
}
