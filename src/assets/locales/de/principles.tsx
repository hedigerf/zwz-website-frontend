export default {
	'principles.content': `Wir setzen uns für einen Raum mit vielfältigen Nutzungen ein, in dem ausprobiert und experimentiert werden kann. 

Insbesondere Menschen, Kollektiven, Initiativen und Projekten, welche dies tun und noch keinen eigenen festen Platz in der Stadt Zürich haben, sollen in der ZW Raum finden. 
	
Wir wollen uns über das was im Raum passiert konstruktiv mit politischen und gesellschaftlichen Fragen auseinandersetzen.
	
Wir agieren nicht gewinnorientiert und es gibt keinen Konsumzwang. Wir streben an, selbsttragend zu sein, ohne dass fehlendes Geld für die Nutzung der Räumlichkeiten ein überwindbares Hindernis darstellen sollte.
	
Zugänglichkeit ist für uns eines der Kernthemen des Projekts. Für neu Dazukommende, welche sich den Grundsätzen anschliessen, soll immer ein möglichst niederschwelliger Zugang möglich sein, um den Raum und dessen Strukturen aktiv mitzugestalten. 
	
Wir möchten Austausch, Synergien, Kollaborationen und Interaktionen fördern - zwischen dem Verein, Veranstalter*innen, Besucher*innen, Nachbar*innen und weiteren Interessierten.
	
Es soll immer Platz haben über Bedürfnisse, Wünsche und Differenzen zu reden. Anliegen von anderen soll zugehört, als auch eigene Anliegen eingebracht werden.
	
Wir wünschen uns einen Umgang welcher geprägt ist von gegenseitigem Respekt und in welchem das Miteinander im Zentrum steht.
	
Wir wollen alle Strukturen des Projekts regelmässig reflektieren und gegebenenfalls anpassen.
	
Wir kreieren einen einladenden Raum, in dem vieles passieren darf und der sich aktiv um offene Teilhabe von immer neuen Personen einsetzt. Wir schaffen einen Möglichkeitsraum, der auch von Besuchenden aktiv mitgestaltet und erlebt werden kann.
	
Diskriminierung insb. in Form von Sexismus, Nationalismus, Rassismus und Homophobie haben keinen Platz. Stattdessen ist der Umgang geprägt von Achtsamkeit und gegenseitigem Respekt.
	
Im Verein Zentralwäscherei agieren die Mitglieder und externen Veranstalter*innen mit einer nicht-gewinnorientierten Haltung - mit Aktivitäten in der Zentralwäscherei darf keine finanzielle/ökonomische Bereicherung einzelner Personen stattfinden. 
	
Wir suchen einen reflexiven und selbstkritischen Umgang mit politischen und gesellschaftlichen Themen, Situationen und Mustern. Wir reflektieren unsere eigenen Machtstrukturen kritisch und sind offen, unser eigenes Handeln diesen Reflexionen anzupassen.
	
Wir streben eine Diversität im Betrieb, dessen Struktur und dem daraus hervorgehenden Programm an. Diversität wünschen wir uns in den Bereichen Geschlecht, Alter, soziale, kulturelle und ethnische Herkunft und sexueller Orientierung. 
	
Die Zentralwäscherei soll für alle interessierten Personen zugänglich sein, die mit unseren Grundsätzen übereinstimmen. Die Diversität zu fördern bedeutet, Verantwortung zu übernehmen und geeignete Massnahmen zu ergreifen, um Diskriminierung zu verhindern und Anerkennung von Diversität zu fördern.`,
}
