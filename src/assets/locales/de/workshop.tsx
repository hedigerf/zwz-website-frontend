export default {
	'workshop.content': `- Unser Ziel ist gemeinsam eine selbstorganisierte, möglichst zugängliche Werkstatt zu schaffen.
- Sie ist zugänglich für alle Personen mit einem jeweiligen Einführungskurs. 
- Alle Personen die freien Zugang zu den Werkstätten haben, tragen Sorge zum Raum. Diese Personen bilden zusammen eine Gruppe in welcher zur Infrastruktur und zueinander geschaut wird.
---
- **Bei Interesse zu Kollaboration**
- [naehen@zentralwaescherei.space](mailto:naehen@zentralwaescherei.space)
- [siebdrucken@zentralwaescherei.space](mailto:siebdrucken@zentralwaescherei.space)
---
- Möchtest du nicht Teil der Gruppe werden, sondern die Werkstatt einmalig benützen, kannst du ebenfalls ein Mail schreiben & wir schauen mal was sich machen lässt. `,
}
