export default {
	'gastro.pre-menu': `Die Kleinwäscherei (ZW-K) ist der Dreh- und Angelpunkt des Kulturraums «Zentralwäscherei». Hier findest du unser Bistro in dem du kulinarisch & kulturell verköstigt wirst.

Ein Platz für entspannte & angeregte Diskussionen, ein Ort des Zusammenseins, Ruhepol & Schmelztiegel. 

Am Tag eher ruhig, am Abend eher laut. 

Unsere Mittagsküche ist unter der Woche jeweils Dienstag bis Freitag geöffnet. Unsere Abendküche hat jeweils von Dienstag bis Samstag offen.

---
- **Reservationen**
- [reservieren@zentralwaescherei.space](mailto:reservieren@zentralwaescherei.space)

---
- **Öffnungszeiten**
- Montag: geschlossen
- Dienstag: 11:30 - 23:00
- Mittwoch: 11:30 - 23:00
- Donnerstag: 11:30 - 03:00
- Freitag: 11:30 - 03:00
- Samstag: 13:00 - 03:00
- Sonntag: 16:00 - 23:00
---`,
	'gastro.gastro-title': '**GASTRO**',
	'gastro.gastro-info':
		'Die Kleinwäscherei (ZW-K) ist der Dreh- und Angelpunkt des Kulturraums «Zentralwäscherei». Hier findest du unser Bistro in dem du kulinarisch & kulturell verköstigt wirst.',
	'gastro.menu-title': '**UNSER MENU**',
	'gastro.menu-info':
		'Unsere Mittagsküche ist unter der Woche jeweils Dienstag bis Freitag geöffnet. Unsere Abendküche hat jeweils von Dienstag bis Samstag offen.',
	'gastro.wochenmenu-title': '**WOCHENMENÜ 17 - 21. Mai**',
	'gastro.wochenmenu': 'Miso-glasierte Auberginen mit Tomatenbulgur und Tahinijoghurt',
	'gastro.dienstag-title': '**DIENSTAG 17.05**',
	'gastro.dienstag': 'Zucchini-Küchlein mit Feta und Linsenreis',
	'gastro.mittwoch-title': '**MITTWOCH 18.05**',
	'gastro.mittwoch': `Risotto mit frischen Erbsen, roten Zwiebeln, Zitrone, Sauerampfer und Belper Knolle`,
	'gastro.donnerstag-title': '**DONNERSTAG 19.05**',
	'gastro.donnerstag': `Grüner Spargel auf Ofenkartoffeln, Mönchsbart, Kumquat, Baumnuss und Aïoli`,
	'gastro.freitag-title': '**FREITAG 20.05**',
	'gastro.freitag': `Frischer Glasnudelsalat mit Zitronengras-Tofu, Pilzen, Gurken und Koriander`,
	'gastro.samstag-title': '**SAMSTAG 21.05**',
	'gastro.samstag': 'Handgemachte Falafel mit Fladenbrot, Auberginenkaviar und Pickles',
	'gastro.drinks-title': '**DRINK**',
	'gastro.drinks':
		'Die Kleinwäscherei bietet Platz für Drinks und entspannte oder angeregte Diskussionen. Ein Ort des Zusammenseins. Ruhepol & Schmelztiegel. Am Tag eher ruhig, am Abend eher laut.',
	'gastro.work-title': '**WORK**',
	'gastro.work':
		'Dienstag bis Samstagnachmittag findet ihr bei uns immer eine ruhige Ecke für Meetings oder zum Arbeiten. Der Chai ist bestellt, bis dahin könnt ihr mit feinen Kräuter Tees von Al Canton oder einem Cafe Noir eure Sinne schärfen.',
	'gastro.post-menu': `---
- **Reservationen**
- [reservieren@zentralwaescherei.space](mailto:reservieren@zentralwaescherei.space)
	
---
- **Öffnungszeiten**
- Montag: geschlossen
- Dienstag: 11:30 - 23:00
- Mittwoch: 11:30 - 23:00
- Donnerstag: 11:30 - 03:00
- Freitag: 11:30 - 03:00
- Samstag: 13:00 - 03:00
- Sonntag: 16:00 - 23:00

---
- **Öffnungszeiten Feiertage 2022**
- *Neujahr*
- 1.1. 07:00 - 23:00 (dayrave)
- 2.1. geschlossen
- *Ostern*
- Fr 15.4. 17:30 - 02:00 mit Abendküche
- Sa 16.4. 17:30 - 06:00 mit Abendküche
- So 17.4. Ping Pong Turnier 13:00 - 23:00
- *1.Mai*
- So, 1.5. geschlossen - alli a d'Demo
- *Auffahrt*
- Do 26.5. 17:30 - 02:00 mit Abendküche
- Fr 27.5. 17:30 - 02:00 mit Abendküche`,
}
