export default {
	'host.content': `Wir wollen neben eigenen Veranstaltungen auch aktiv mit anderen Organisationen und Kollektiven Koproduktionen eingehen. Beispiele für Veranstaltungs-Koproduktionen können Ausstellungen, soziale Formate, diskursive Formate, politische Veranstaltungen, Konzerte, Theater, Installationen, Safe-Spaces und Soundperformances sein. Ausgeschlossen sind Veranstaltungen die gegen unsere Grundsätze verstossen, Privatveranstaltungen oder Parties.

Vier Mal im Jahr setzen wir Deadlines an, bis zu denen ihr eure Projekte einreichen könnt. **IM MOMENT IST DER OPEN CALL GESCHLOSSEN**.
	
Wir berechnen euch dann einen Kooperationsbeitrag, welcher sich aus verschiedensten Kriterien wie Raumnutzung, Infrastrukturnutzung, Wochentage, Förderungswürdigkeit und einer Beteiligung an den variablen Kosten zusammensetzt. Es werden nur Tage der Aufführungen und nicht die Proben/Aufbauten (bei Ausstellungen nur die Vernissagen/Finissagen) berechnet. Der Betrag befindet sich je nach Mietschlüssel zwischen 50-500 CHF pro Tag. Die Berechnungsgrundlage für den Kooperationsbeitrag findet sich [HIER](https://docs.google.com/spreadsheets/d/1gPdp_VA5fN9x6iogIXWDp7VauhSm9xeupR6uA649_FU/edit?usp=sharing).
	
Es sind keine Vorkenntnisse nötig! Jegliche Erfahrungsstufen sind willkommen!
	
Bei Interesse kannst du unsere Grundsätze lesen und anschliessend den untenstehenden Fragebogen ausfüllen. Reiche uns einen Projektbeschrieb und wenn möglich ein Budgetplan ein.

Wir werden uns nach der Deadline eine Übersicht über die Projekte verschaffen und in einer Planungswoche über die Projekte befinden. Bis Ende Januar werdet ihr von uns eine Rückmeldung erhalten. Die Richtlinien, welche unsere Entscheidungsgrundlage bilden, finden sich [hier](https://docs.google.com/document/d/1qi86Ok7bYnqdsOi6ARaKFe8I_MJABOVZJ9ZSk3GoH50/edit?usp=sharing).
	
Bei weiteren Fragen [programm@zentralwaescherei.space](mailto:programm@zentralwaescherei.space) kontaktieren.
	
Mit einer Anmeldung gibt sich der/die Veranstaltende einverstanden mit unseren [Grundsätzen](/principles).`,
}
