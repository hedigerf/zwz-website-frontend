export default {
	'opening.content': 'Opening',
	'opening.header': `Wir sind der Verein Zentralwäscherei. Wir eröffnen einen unkommerziellen Kulturbetrieb im Kreis 5 in dem alle mitwirken können. Kommt ans Eröffnungsfestival und wieder zurück.`,
	'opening.text': `Einlass nur mit Covidzertifizierung möglich. Wir sind zudem verpflichtet eure Daten für das Contacttracing aufzunehmen. Diese werden nach 14 Tagen gelöscht. *[COVID Info](/covid)*

Kein Vorverkauf. Tickets sind an der Abendkasse zu einen Richtpreis erhältlich. (Mehr Infos bezüglich Richtpreis siehe *[Über Uns](/about)*). First come, first save. Einlass ab 18 J. bei Partynacht am Festival.

Bitte verhalte dich so, wie du es dir von Anderen wünschen würdest. Lies dir unser *[Awarenesskonzept](/awareness)* durch.`,
	'opening.chichiezha': `**"drzwüsche”**

Ein Stück, welches sich mit der Herausforderung der Schaffung einer Identität in der Dualität von Kulturen beschäftigt. Es ist eine Aufarbeitung von Erfahrungen und ein Austausch von Erlebnissen.

Es ist eine Hommage an alle die sich «drzwüsche» fühlen, für die Menschen mit Migrationshintergrund, welche lernen zwischen der Kultur ihrer Eltern und der schweizerischen Kultur zu balancieren, für die, deren Eltern aus zwei verschiedenen Ländern stammen und sich in beiden Ländern zu Hause oder/und fremd fühlen. Wir sind alle in einer Form dazwischen, denn keine Identität ist statisch. Das Gefühl überall und nirgends ganz dazuzugehören kennen die meisten.

Dieses Stück wurde von Anna Chiedza Spörri und Muhammed Kaltuk choreographiert und nach der Premiere im Jahr 2019 am Bestival auch am Theater Basel und in der Dampfzentrale aufgeführt. Nun ist es in einer Kurzversion und zu Zweit auf Tour und hinterfragt  mit Text und Tanz Miccroaggressionen und Rassismus im Alltag.

**Annakatharina Chiedza Spörri**

Anna ist seit über 10 Jahren aktiv in der Berner Tanzszene. Angefangen hat sie mit Hip- Hop, House und Ragga und entwickelte diese Stile alle ausgiebig weiter. 2013 besuchte sie in New York dier Peri Dance Schule. Sie gründete die Tanzgruppen “Urban Rebels” und “Unusual”. Ausserdem ist sie Mitinitiatorin von “Limelight”, einer Plattform für junge ChoreografInnen in der Schweiz. 

Seit 2019 war sie als Choreografin mit verschiedenen Stücken am Theater Basel, dem Tojo Theater und in der Dampfzentrale Bern zu Gast. 

**Sophie Gerber**

Sophie hat letztes Jahr das fünf jährige Sport Gymnasium mit dem Schwerpunkt Tanz abgeschlossen. Sie hat während dem intensiv getanzt und sich in diversen Stilen weitergebildet. Seit zwei Jahren ist sie aktiv als professionelle Tänzerin in der Schweiz unterwegs und hat schon in diversen Produktionen mitgetanzt. Seit September 2020 studiert sie an der Höhere Fachschule fur Zeitgenössischen und Urbanen Bühnentanz im Tanzwerk`,
	'opening.titilay': `Titilayo Adebayo ist eine nonkonforme Performerin, Bewegungs- und Tanzkünstlerin, die sich im Fluss des Aufbruchs befindet.  Wohin aufbrechend? Malen im Rhythmus? Sprechen ohne Worte? Wer weiß das schon? Ist das wichtig? Ihr Ziel ist es, einen ganzheitlichen Ansatz für das Schaffen und Erleben von Performance, visueller und erfahrungsbezogener Kunst zu kreieren. Ihre Praxis beruht auf diesen Grundsätzen.`,
	'opening.yuul': `Yuul aka Yael Weinberg ist eine junge talentierte DJ aus dem Regula - Kollektiv. Ihr Stil ist als eklektisch zu bezeichnen, denn sie fühlt sichi in mehreren Genres wohl und scheut sich nicht diese zu dekonstruieren. `,
	'opening.workshopf96': `**Workshop Musikproduktion von F96**

In diesem Workshop geben verschiedene erfahrene KünstlerInnen Einblick in die Produktion Elektronischer Musik. Gehe Erste Schritte mit Stepsequenzern und mach es dir auf Filterbänken bequem. Kein Vorwissen notwenig - dieser Workshop ist als Icebreaker gedacht!

**Konkret geht es darum, die folgenden Fähigkeiten zu erlernen:**

- Die Grundlagen der Erstellung eines eigenen Tracks kennen (Beats, Basslines, Melodien, Sampling)
- Ein neues Projekt in Ableton* (beliebte Software für die Produktion elektronischer Musik) einrichten
- Experimentieren mit Drumcomputern und Synthesizern 
- Zusammen mit einem Mentor ein Projekt erstellen 
---
Eine Grundausrüstung wird zur Verfügung gestellt. Die Teilnehmer werden gebeten, einen Laptop und Kopfhörer mitzubringen, falls vorhanden. Wenn Ihr keinen Laptop mitbringen können, setzt euch mit uns in Verbindung damit wir gemeinsam eine Lösung finden. Ausserdem werden Synthesizer und Drumcomputer, welche autonom ohne Laptop funktionieren, zur Verfügung gestellt. 

**Zeitplan**

- 16:00 - 16:45 Einführung 
- 16:45 - 17:30 Einrichten eines Projekts mit einem Mentor 
- 17:30 - 18:30 Freie Zeit/ Arbeit an Tracks/ Jammen mit Hardware 
- 18:30 - 19:00 Reflektion im Clubraum 
---
**Anmeldung**
	
Der Platz im Workshop ist begrenzt (max. 15.), bitte meldet euch sich rechtzeitig an: ***[Anmeldeformular](https://docs.google.com/forms/d/e/1FAIpQLSeMWlXYgcvjqeQoxyj2Y_uezLsYT05DvdXBvRkfAY0msO0g7A/viewform?usp=sf_link)***
Falls ihr Eure anmeldung stornieren müsst, informiert uns bitte 1 Woche vor dem Workshop, damit wir die Platz an jemanden von der Warteliste vergeben können. FINTA, LGBTQIA+, Queer oder BiPoc Personen werden bevorzugt behandelt. 
	
**Aufruf für Mentoren/Equipment**
	
Falls du Ausrüstung oder Kenntnisse der Musikproduktion beisteuern kannst, melde dich doch über dieses _[Sheet](https://forms.gle/P3LZHkqQMsTVURhG8vv)_.
Die Mentoren werden mit jeweils 2-3 Teilnehmern gepaart, um ihnen mit ihrem Ableton-Projekts zu helfen und bei Bedarf Fragen zu beantworten und Unterstützung zu leisten. Als kleines Dankeschön kriegen die Mentoren eine Freikarte für das ZZZZestival.
	
**F96**

F96 ist ein feministisches Kollektiv. Sie fordern ein diverses Kulturleben und streben danach, dieses aktiv mitzugestalten. Sie motivieren und unterstützen sich und andere beim Denken und Umsetzen von Ideen und im Austausch und Erweitern von Wissen.
	
F96 wächst und schrumpft kontinuierlich und dynamisch – FINTA*, die das Kollektiv aktiv mitgestalten wollen, sind jederzeit herzlich willkommen. Wir stehen auf den Schultern aller, die vor uns kamen und wollen gemeinsam mit verschiedenen Akteurinnen die Zukunft neu gestalten.`,
	'opening.nodin': `**Work-in-Progress-Sharing: “I am many, I contains multitudes.”**

- _Die Wirbelsäule ist von Flüssigkeit umgeben_
- _Und pulsiert in verschiedenen Rhythmen._
- _Liquor cerebrospinalis amen. Dura mater amen._

---

_I am many, I contains multitudes._ Vier Performer:innen teilen einen Raum, doch sie sind weitaus mehr. Ihre multitudes sind zahlreich, entstehen in der Intraaktion1 und sind nicht im Individuum gehalten. Ein Zwischenstopp, Ein Innehalten; Gefühle treten an die Oberfläche, Impulse verstärken sich, Ansichten werden geklärt. Die Performer*innen teilen diese Momente. In der Intraaktion verflüssigen sich ihre Identitäten, suchen Halt in der Nasszelle. Trauma of the leak. Meine Gefühle schwappen über. Meine Gefühle floppen over.

---
- _Liquor cerebrospinalis amen. Dura mater amen._
- _Den verschiedenen Strömungen horchen,_
- _einen geteilten Impuls suchen, beten_
- _für eine gemeinsame Moral._
---

In der Performance I am many. I contains multitudes. suchen das Kollektiv “noDIN” nach Identitäten im Prozess des Teilens. Was können wir vom Teilen lernen? – über unsere Bedürfnisse, unsere eigenen Wiedersprüche, über unsere Ambiguitäten?
	
1 Der Begriff stammt von Karen Barad. Er kann als aktualisierte Version des Begriffes der Interaktion verstanden werden und leitet gleichzeitig eine alternative Weltansicht ein, in der es keine autonomen Individuen gibt, die miteinander interagieren, sondern in der alle Wesen erst in dem sie aufeinandertreffen, durch ihre Unterschiede zusammen-werden.
	
**noDIN**
- 4 Performer*innen bilden zusammen das noDIN Kollektiv.
- Mira Studer, Yadin Bernauer, Dustin Kenel und Sophie Germanier.
- Ihre verschiedenen Disziplinen und Arbeitsweisen stossen aufeinander, verschmelzen miteinander und teilen sich dabei wieder auf.
---
**Credits**

Das Projekt wird begleitet von Danse & Dramaturgie (D&D CH), ein Programm initiiert von Théâtre Sévelin 36 Lausanne, in Zusammenarbeit mit der Dampfzentrale Bern, ROXY Birsfelden, Südpol Luzern, Tanzhaus Zürich, TU-Théâtre de l’Usine Genève; gefördert durch Pro Helvetia und SSA Société Suisse des Auteurs.
	`,
	'opening.megahex': `Megahex ist ein subjektives, politisches, radikales und dezentrales Internet-Radio aus Zureich.  Megahex informiert über linke Widerstandskämpfe, klärt Misstände auf und sendet Musiksendungen von unabhängigen Kollektiven/Künstler*innen. Für das ZW Opening übernimmt Megahex das Streaminghosting und produziert auch drei Eigensendungen live!`,
	'opening.xerosophie': `Mehr Informationen in Kürze.`,
	'opening.emzyg': `Emzyg ist eine fünfköpfige Zürcher Band, frisch aber äusserst stilsicher. Ein Mix aus dreamy psychedelic, garage punk und Post-Punk, den du am Besten mir deiner Sonnenbrille auf geniesst.`,
	'opening.dorota': `Dorota Gawęda (PL) und Eglė Kulbokaitė (LT) sind ein Künstler:innenduo mit Sitz in Basel (CH). Beide sind Absolvent:innen des Royal College of Art in London. Ihr Werk umfasst Performance, Skulptur, Malerei, Fotografie, Parfum und Video. Gawęda und Kulbokaitė sind Preisträger:innen des Swiss Performance Art Award 2021 und unter den Finalist:innen des Swiss Art Awards 2021, sie sind auch für den Prix Mobilière 2022 nominiert. Sie haben kürzlich ihre erste Monografie bei Edizioni Periferia & Pro Helvetia veröffentlicht und sind die Gründerinnen der YOUNG GIRL READING GROUP (2013-2021).`,
	'opening.nico': `Unser Hausinterner Libero Nico Sun nun endlich in der Rolle, für die er heimlich so hart gearbeitet hat: selbst im Rampenlicht. Ein wenig Bescheidenheit verbleibt jedoch, er holt sich Unterstützung von Angelo Repetto, der einen Hälfte des Zürcher Electropopduos “Wolfman”, welcher in letzter Zeit etwas mehr Zeit zum üben hatte. Gemeinsam präsentieren sie ausgewählte Exemplare ihrer Synthesitzerkollektion und spielen ein Liveset zwischen Krautrock und Ambient. `,
	'opening.mavichan': `Mehr Informationen in Kürze.`,
	'opening.judallas': `Ju Dallas ist ein junges, aufstrebendes DJ-Talent aus Zürich. Sie ist aktiv in verschiedenen Projekten, wie z.B dem Label und Platform Axophono zusammen mit Sean Douglas und der kollektiven Platform Anaram Records. Sicherlich eine Protagonistin des Zürcher Nachtlebens von der wir noch viel hören werden.`,
	'opening.siebdruck': `Mehr Informationen in Kürze.`,
	'opening.voguenight': `Das Haus  B. Poderosa ist Gastgeber*in dieser Kiki Vogue Night.

Vogue-Nächte sind Teil der Ballsaal-Szene, die in den 70er Jahren in New York von schwarzen, lateinamerikanischen, transsexuellen und queeren Gemeinschaften in den 70er Jahren in New York gegründet wurde. Da es sich um eine Vogue Night und nicht um einen Ball handelt, gibt es kein bestimmtes Thema, so dass du weniger Zeit für die Vorbereitung aufwenden musst und einfach kommen kannst, um der Jury dein Können zu zeigen, in der Kategoire in welcher du dich am wohlsten fühlst.

Die Kategorien sind: Runway OTA, Vogue Femme OTA, Shake that Ass OTA, Face OTA

Nach den Kategorien gibt es eine tolle Party für euch mit unseren DJs Klair-Alice und DJ Essengo.`,
	'opening.maninsandals': `Maxi - man in sandals - führt, einmal in Birkenstöcken hinter Plattenteller gestellt, mit hoher Konzentration durch seine Sammlung an erdigem Ambient. `,
	'opening.afterparty': `Mehr Informationen in Kürze.`,
	'opening.regula': `Regula ist eine Freundesgruppe, die sich zusammengefunden hat, um Happenings rund um elektronische Musik zu organisieren. Sie realisieren Veranstaltungen, die ihren eigenen musikalischen, kreativen und gemeinschaftlichen Ideen entsprechen. Mehrere Mitglieder beschäftigen sich auch künstlerisch mit elektronischer Musik, entweder als Live-Musiker, DJs oder Produzenten. Regulas musikalisches Schaffen ist nicht so sehr durch Genregrenzen, sondern eher durch gemeinsame ästhetische Ideen definiert. Da sie aus der Rave- und Clubkultur kommen, sind diese musikalischen Wurzeln immer noch relevant und stehen im Mittelpunkt der meisten ihrer Arbeiten. Doch ihr musikalischer Fussabdruck entwickelt sich weiter, sie beschäftigen sich mit verschiedenen Formen der musikalischer Kreation und Präsentation wie Klanginstallationen, Ambient-Produktionen oder experimentelle Listeningsessions.`,
	'opening.neoele': `**Glitzerwunsch**

Funkeln, schimmern und glitzern in allen möglichen Farben – zusammen basteln wir eine märchenhafte Welt im Glas und lassen den Schnee im faszinierenden Glitzerglas herumwirbeln und uns damit verzaubern.
	
Um 15.30 Uhr – Erzählung des Märchens *Sterntaler* (Brüder Grimm) für alle unsere kleinen und natürlich auch die grossen Besucher*innen. Das Märchen handelt von Hilfsbereitschaft, Vertrauen, Mut und Gemeinschaftsgefühl.`,
	'opening.erektion': `**Ive looked at love from both sides now oder ein permanenter zustand der erektion**

- What is love, baby don’t hurt me, don’t hurt me, no more ...
- Du darfst mich ansehen. Ich will, dass du mich ansiehst. I want you to look at me. My body is ready to be watched, consumable. Verfügbar. Setz dich. Sieh hin. Tauch ein in ein Paradies der nie endenden Love-Experience. Search no more. Wir sind wonach du dich sehnst. Diese Welt ist perfect und shiny.
- Tasty Songs, tasty bodies, tasty action – come and look at our faces!
---
Eine Performance über Liebe von und mit Joel Kammermann, Nils Nikolas Klaus, Isabella Roumiantsev, Stefanie Steffen, Marie Nest, Janna Rottmann, Fynn Malte Schmidt, Timo Raddatz und Emanuel Steffen`,
	'opening.nnz': `Die Krönung der Zürcher Zeitungslandschaft druckt ihre dritte Ausgabe. Ja und Jetzt? Die Neue Neue Zeitung macht am Samstag Soft-Vernissage, das erste Mal live zum anfassen. Hänki, Mara, Sophie und Jeannie sitzen auf und lesen aus ihren Gratiszeitungen, die ihr exklusiv zu einem Solipreis von 5.- kaufen könnt. Vor allem aber könnt ihr mit ihnen schwatzen und lachen und andere lustige Sachen machen. Es gibt temporary Tattoos und Erinnerungen, die für immer bleiben.`,
	'opening.babylon': `Babylon Music ist ein junges Musikkollektiv aus dem Herzen Zürichs, unter anderen bestehend aus den drei Musikern Akira, P Vlex und Jaron Ivy. Von der Entstehung der Beats über das Konzept der Liveshows bis zum Schneiden der Videos: Bei Babylon Music bleibt alles DIY und in der Familie. Mittlerweile besitzt Babylon einen beachtlichen Katalog an Produktionen, in welchem “Flores” die neuste Veröffentlichung darstellt.`,
	'opening.naim': `Der Basler Künstler Heggi aka Naim schafft es, in jedem Projekt anders zu klingen, ohne seine Handschrift zu verlieren. In seiner Musik treffen Hip-Hop- und Trap-Elemente auf elektronische und auch Rockeinflüsse. Ästhetisch spielt er auch mit verschiedenen Subkulturen und als Inspiration nannte er einmal Led Zeppelin - was seine musikalische Offenheit unterstreicht.`,
	'opening.cobee': `Cobee hat mit seinen Ersten Singlereleases und seinem Debut Album “Chaos” ziemlich schnell die Grenzen des Mundartraps ausgelotet. Nun versucht er sich sprachlich wie musikalisch in neuen Spähren. Die Beats sind reduzierter, unternehmen gerne mal Ausschweife weg vom klassischen Trapnarrativ. Die Texte sind neu auf Hochdeutsch, was universell klingt, und bestimmt auch klingen soll. Ein Meister des Chaos, Genrebeschreibungen tun  sich äusserst schwer (Trap-Cloud-Hop-Free-Flow!), zu Ersten Mal mit seinem neuen Projekt live zu hören!`,
	'opening.tvbxs': `TVBXS ist eine sehr engagierte Person im Zürcher Nachtleben und eine grossartige DJ. Sie ist teil des Kollektives TBD, welches verschiedene Zugänge im Nachtleben für weniger priviligierte Menschen schafft. Ihre Partynacht “putasfinas” verfolgt den gleichen sozio-politischen Aktivismus und versucht somit  diverse Menschenan verschiedenen Fronten zu inkludieren.`,
	'opening.vielzuhell': `Das Licht- und Visualkollektiv VielZuHell, bestehend aus den Künstlern Timo Raddatz, Hannah Gottschalk und Lukas Matthys hat seine Basis in der Zürcher Photobastei, von wo aus sie die wildesten Lösungen für Lichtdesign und Visuals in die Welt hinaustragen. Von Konzerten verschiedenster Genres bis über zu Techno-Raves und Kunst Installationen, ihre Arbeiten zeichnen sich stets durch Abstraktion und Audio-Reaktivität aus, und der dadurch kreierten Symbiose der musikalischen und visuellen Wahrnehmung.`,
	'opening.sellyourmania': `Sellyourmania verkauft euch ganz pragmatisch einiges: Leftfield Electro/Breakbeat und Halftime Acid! Seid auf der Hut.`,
	'opening.chriskorda': `Chris Korda (geb. 1962) ist eine amerikanische antinatalistischer Aktivistin, Anführerin einer transsexuellen Selbstmordsekte, Komponistin elektronischer Musik, digitale Künstlerin, Entwicklerin freier Software und vegane Anführerin der Church of Euthanasia. Die Church of Euthanasia ist eine in Massachusetts ansässige Organisation, die dafür eintritt, die Überbevölkerung der Erde durch ihre vier Säulen "Selbstmord, Abtreibung, Kannibalismus und Sodomie" zu stoppen.`,
	'opening.siegwart': `Seit einer gefühlten Ewigkeit bespielt der Zürcher Siegwart die Clubs in und um Zürich. Er gilt als Djs Dj und besticht immer wieder durch seine feinfühlig kuratierten Sets. Von Cosmic bis Edgy.`,
	'opening.djweb': `Walid El Barbir alias DJ W.E.B ist Teil des Label Les Points, dem Kollektiv & Space Mikro und Gründer des WEBarchive. W.E.B. ist bekannt als ein sehr obskurer, genrebrechender Produzent und engagierter nischengenre DJ. Zusammen mit seinen Mitstreitern pionierte er einen Genre-Mix, der Jahre später eine Massenadaption fand und sich in Aktivierungsmomenten für viele jüngere Menschen manifestierte - meist durch die Organisation und das Bespielen der illegalen Raves im Mikro.`,
	'opening.katzele': `Katzele, Gründer von Malka Tuti Records, ist bekannt für lange, energetische Sets. Er verwebt Drone, verspielte Melodien und esoterische Konzepte mit langsamen und schweren Beats die lange Klangreisen auf der Tanzfläche erzeugen.`,
	'opening.amygdala': `Anissa Todesco alias Amygdala ist eine Schweizer DJ, Produzentin, Multiinstrumentalistin und Plattensammlerin. Amygdalas Tracks und DJ-Sets sind ohne Genre- oder Tempobeschränkung ein Feuerwerk der Emotionen. Ihr Stil kombiniert Proto-Acid-Punk, frühe Leftfield-Musik und Drum & Bass mit ansteckenden Techno-Rhythmen und aggressiven Industrial-Bops.`,
}
