export default {
	'about.content': `Wir sind der Verein Zentralwäscherei und betreiben im Kreis 5 den unkommerziellen Kultur- und Begegnungsraum Zentralwäscherei. Wir streben einen möglichst inklusiven Raum für Begegnung, Kunst, Kultur und Gastronomie an. Ein Raum für Diskussionen, für Öffentlichkeit, für Konsum, für Tanz, um selbst zu präsentieren, um das Leben zu feiern, zu hinterfragen und Spass zu haben.
	
---

Wir sind der Meinung, dass Kultur für möglichst viele zugänglich sein sollte. Aus dieser Überzeugung heraus bieten wir bei Eintritten für kulturelle Veranstaltungen Richtpreise an. Als Richtpreise verstehen wir empfohlene Preise, von denen abgewichen werden kann. Dies insbesondere, damit der Besuch der Veranstaltungen auch für Menschen mit prekären finanziellen Verhältnissen möglich ist. 
Da Kultur ihren Wert hat und wir als unkommerzieller Kulturbetrieb weiterhin bestehen bleiben können, sind wir jedoch auch auf angemessene Eintrittsbeiträge angewiesen.

---

Unser Betrieb umfasst folgende Räumlichkeiten:
	
**ZW-K** Die Kleinwäscherei ist unser Gastrobetrieb. 
	
**ZW-H** Die grosse Halle ist unser Veranstaltungsraum, der Platz für verschiedenste Veranstaltungsformate wie Podiumsdiskussionen, Theater, Performances und so weiter. 
	
**ZW-B** Der Beschallungsraum bietet Platz für Musikveranstaltungen wie Konzerte, Raves und Sound Performances. 
	
**ZW-W** Unser Werkbereich verfügt über eine Siebdruck- und eine Nähwerkstatt. Eine Holzwerkstatt ist in Planung. Diese können nach einem Einführungskurs von Allen benutzt werden.

---

- Der Verein ist ein stetig wachsendes Kollektiv von momentan circa 150 Mitgliedern. 
- Möchtest du dich engagieren? 
- Unser “wir” ist immer offen für Neue!`,
}
