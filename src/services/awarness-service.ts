// Or from '@reduxjs/toolkit/query/react'
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { Incident } from './awarness.routes.d'
import { baseUrlApi } from './base'

export const awarnessApi = createApi({
	reducerPath: 'awarnessApi',
	baseQuery: fetchBaseQuery({ baseUrl: baseUrlApi }),
	endpoints: (builder) => ({
		createIncident: builder.mutation<Incident, Incident>({
			query: (data: Incident) => ({
				url: '/awarness/incidents',
				method: 'post',
				body: data,
			}),
		}),
	}),
})

export const { useCreateIncidentMutation } = awarnessApi
