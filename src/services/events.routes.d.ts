export type EventLinkName = 'self' | 'next' | 'previous' | 'images'

export type EventsLinkName = 'self'

export type PartialEventLinkName = 'self' | 'image'

export type Link = {
	href: string
}

export type Links = Record<string, Link>

export type EventLinks = Record<EventLinkName, Link | Link[]>

export type EventsLinks = Record<EventsLinkName, Link>

export type PartialEventLinks = Record<PartialEventLinkName, Link>

export interface PartialEvent extends HalRepresentation<PartialEventLinks> {
	id?: string
	title: string
	fromDate: number
	toDate: number
	type: string
	tags: Array<string>
	location: Array<string>
	imgUrl?: string
}

export type EventsEmbedded = {
	events: PartialEvent[]
}

export interface HalRepresentation<T extends Links> {
	_links: T
}

export interface HalEmbedded<T> {
	_embedded: T
}

export interface EventType extends HalRepresentation<EventLinks> {
	id?: string
	title: string
	fromDate: number
	toDate: number
	type: string
	tags: Array<string>
	location: Array<string>
	description: string
	lineup: Array<string>
	ticketUrl: string
	imgUrls: Array[string]
}

export interface Events extends HalRepresentation<EventsLinks>, HalEmbedded<EventsEmbedded> {}
