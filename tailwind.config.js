module.exports = {
	purge: { enabled: true, content: ['./src/**/*.scss', './src/**/*.tsx', './src/**/*.ts'] },
	darkMode: false, // or 'media' or 'class'
	theme: {
		extend: {
			gridTemplateColumns: {
				nav: 'repeat(auto-fill, 14.28vw)',
				'nav-md': 'repeat(auto-fill, 20vw)',
				'nav-sm': 'repeat(auto-fill, 33.33vw)',
			},
			gridAutoColumns: {
				nav: 'minmax(14.28vw, 14.28vw)',
				'nav-md': 'minmax(20vw, 20vw)',
				'nav-sm': 'minmax(33.33vw, 33.33vw)',
			},
			inset: {
				'1/5': '20%',
				'1/7': '14.28%',
			},
		},
	},
	variants: { extend: {} },
	plugins: [],
}
